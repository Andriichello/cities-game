﻿using Cities_Game;
using Cities_Game.Areas;
using Cities_Game.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cities_Game.MVC
{
    public interface IGamePlay
    {
        void CreateNewGame(uint rows, uint cols, uint towns);
        GameModel.Memento SaveGame();
        void LoadGame(GameModel.Memento memento);
        void RevertToPreviousSave();

        void CellSelected(uint row, uint col);
        void ActionSelected(uint row, uint col, string action);
        void CaptureArea(uint row, uint col);
    }

    // Abstract Component
    public abstract class ControllerComponent : IGamePlay
    {
        protected IMapManagementFacade gameManager;
        protected IGameView gameView;
        protected GameModel gameModel;

        protected ControllerComponent(GameModel gameModel, IGameView gameView)
        {
            this.gameView = gameView;
            this.gameModel = gameModel;
            this.gameManager = new MapManagementFacade(gameModel);
        }

        public void ActionSelected(uint row, uint col, string action)
        {
            Area area = gameModel.GetArea(row, col);
            if (area == null)
                return;

            bool isSuccessfull = false;
            if (action == "Capture Area")
            {
                isSuccessfull = gameManager.CapureArea(row, col);
            }
            else if (action == "Remove Building")
            {
                isSuccessfull = gameManager.RemoveBuilding(row, col);
            }
            else if (action == "Remove Obstacle")
            {
                isSuccessfull = gameManager.RemoveObstacle(row, col);
            }
            else if (action == "Place House")
            {
                isSuccessfull = gameManager.PlaceBuilding(row, col, MapManagementFacade.houseFactory);
            }
            else if (action == "Place Works")
            {
                if (area.GetResource().type == RESOURCE_TYPE.WOOD)
                    isSuccessfull = gameManager.PlaceBuilding(row, col, MapManagementFacade.woodWorksFactory);
                else if (area.GetResource().type == RESOURCE_TYPE.ROCK)
                    isSuccessfull = gameManager.PlaceBuilding(row, col, MapManagementFacade.rockWorksFactory);
                else if (area.GetResource().type == RESOURCE_TYPE.FOOD)
                    isSuccessfull = gameManager.PlaceBuilding(row, col, MapManagementFacade.foodWorksFactory);
            }
            else if (action.Contains("Upgrade"))
            {
                isSuccessfull = gameManager.UpgradeBuilding(row, col);
            }

            if (isSuccessfull == true)
            {
                gameView.ShowTownInfo(GenerateTownInfoList(gameModel.player));
                gameView.ShowCellInfo(GenerateInfoList(row, col));
                gameView.ShowActions(GenerateActionList(row, col));
                gameView.ShowTown(gameModel.towns[(int)gameModel.player].GetPositions(), gameModel.player);

                Position current = new Position((int)row, (int)col);
                gameView.ShowCell(current, gameModel.GetArea(current).ShortStringRepresentation()); 
                CellSelected(row, col);
                //SaveGame();
            }
        }

        public void CaptureArea(uint row, uint col)
        {
            throw new NotImplementedException();
        }

        public void CellSelected(uint row, uint col)
        {
            gameView.ShowCellInfo(GenerateInfoList(row, col));
            gameView.ShowActions(GenerateActionList(row, col));
        }

        public List<string> GenerateInfoList(uint row, uint col)
        {
            List<string> info = null;
            Area area = gameModel.GetArea(row, col);
            if (area == null)
                return null;

            info = new List<string>();
            string data = "";
            if (area.IsThereResource())
            {
                data = "Resource: ";
                info.Add(data + area.GetResource().ToString());
            }
            if (area.IsThereBuilding())
            {
                data = "Building: ";
                info.Add(data + area.GetBuilding().GetType().Name);
                info.Add("   " + "Level: " + area.GetBuilding().GetLevel());
                Resource income = area.GetBuilding().GetIncome(area.GetBuilding().GetLevel());
                if (income != null)
                    info.Add("   " + "Income: " + area.GetBuilding().GetIncome(area.GetBuilding().GetLevel()).ToString());

                info.Add("   " + "Capacity: " + area.GetBuilding().GetWorkers() + "/" + area.GetBuilding().GetCapacity(area.GetBuilding().GetLevel()));
            }
            if (area.IsThereObstacle())
            {
                data = "Obstacle: ";
                info.Add(data + area.GetObstacle().GetResource().ToString());
            }
            return info;
        }

        public List<string> GenerateActionList(uint row, uint col)
        {
            List<string> actions = null;
            List<Position> townProperty = gameModel.towns.ElementAt((int)gameModel.player).GetPositions();
            bool isProperty = false;
            foreach (Position pos in townProperty)
            {
                if (pos.row == row && pos.col == col)
                {
                    isProperty = true;
                    break;
                }
            }

            if (isProperty)
            {
                Area area = gameModel.GetArea(row, col);
                if (area == null)
                    return null;

                actions = new List<string>();
                if (area.IsItEmpty())
                {
                    actions.Add("Place House");
                }
                if (area.IsThereResource())
                {
                    if (area.IsThereBuilding() == false)
                        actions.Add("Place Works");
                }
                if (area.IsThereBuilding())
                {
                    actions.Add("Upgrade " + area.GetBuilding().GetType().Name);
                    actions.Add("Remove Building");
                }
                if (area.IsThereObstacle())
                {
                    actions.Add("Remove Obstacle");
                }
                return actions;
            }


            bool isMove = false;
            List<Area> moves = gameModel.GetAvailableArea(gameModel.player);
            foreach (Area move in moves)
            {
                Position pos = move.GetPosition();
                if (pos.row == row && pos.col == col)
                {
                    isMove = true;
                    break;
                }
            }

            if (isMove)
            {
                if (gameModel.GetArea(row, col).IsItCaptured() == false)
                {
                    actions = new List<string>();
                    actions.Add("Capture Area");
                }
                return actions;
            }
            return null;
        }

        public List<string> GenerateTownInfoList(uint player)
        {
            if (gameModel.towns.Count <= player)
                return null;
            
            List<string> info = new List<string>();
            Town town = gameModel.towns.ElementAt((int)player);
            info.Add("Town " + Convert.ToString(player));
            info.Add("Energy: " + Convert.ToString(gameModel.towns.ElementAt((int)player).mEnergy));
            info.Add("Population: " + Convert.ToString(town.GetPopulation()));
            info.Add("Resources: ");
            foreach (Resource res in town.GetResources())
            {
                info.Add("\t" + res.ToString());
            }
            return info;
        }

        public void ShowMap()
        {
            List<List<string>> playground = new List<List<string>>();
            for (int i = 0; i < gameModel.rows; i++)
            {
                playground.Add(new List<string>());
                for (int j = 0; j < gameModel.columns; j++)
                {
                    playground[i].Add(gameModel.map[i * (int)gameModel.columns + j].ShortStringRepresentation());
                    //playground[i].Add(Convert.ToString(i * cols + j));
                }
            }
            gameView.ShowMap(playground);
            for (int i = 0; i < gameModel.towns.Count; i++)
                gameView.ShowTown(gameModel.towns[i].GetPositions(), (uint)i);

            // showing town information
            gameView.ShowTownInfo(GenerateTownInfoList(0));
        }

        public void CreateNewGame(uint rows, uint cols, uint players)
        {
            gameManager.GenerateMap(rows, cols, players);
            ShowMap();
            SaveGame();
        }

        public abstract void NextPlayer();

        private Stack<GameModel.Memento> mementos = null;
        
        public GameModel.Memento SaveGame()
        {
            if (mementos == null)
                mementos = new Stack<GameModel.Memento>();
            mementos.Push(gameModel.GetMemento());
            return mementos.First();
        }

        public void LoadGame(GameModel.Memento memento)
        {
            if (memento == null)
                return;

            gameModel.RevertToMemento(memento);
            gameView.ClearMap();

            ShowMap();
            //foreach (Area area in gameModel.map)
            //{
            //    gameView.ShowCell(area.GetPosition(), area.ShortStringRepresentation());
            //    CellSelected((uint)area.GetPosition().row, (uint)area.GetPosition().col);
            //}

            for (int i = 0; i < gameModel.towns.Count; i++)
                gameView.ShowTown(gameModel.towns[i].GetPositions(), (uint)i);

            gameView.ShowCellInfo(null);
            gameView.ShowActions(null);
            gameView.ShowTownInfo(GenerateTownInfoList(0));
        }

        public void RevertToPreviousSave()
        {
            if (mementos == null)
                return;
            if (mementos.Count == 0)
                return;

            gameModel.RevertToMemento(mementos.Pop());
            gameView.ClearMap();

            foreach (Area area in gameModel.map)
            {
                gameView.ShowCell(area.GetPosition(), area.ShortStringRepresentation());
                CellSelected((uint)area.GetPosition().row, (uint)area.GetPosition().col);
            }

            for (int i = 0; i < gameModel.towns.Count; i++)
                gameView.ShowTown(gameModel.towns[i].GetPositions(), (uint) i);

            gameView.ShowCellInfo(null);
            gameView.ShowActions(null);
            gameView.ShowTownInfo(GenerateTownInfoList(gameModel.player));
        }

    }

    // Concrete Component
    public class DefaultGameController : ControllerComponent
    {
        public DefaultGameController(GameModel gameModel, IGameView gameView) : base(gameModel, gameView)
        {

        }

        public override void NextPlayer()
        {
            // this method will just give the right to move to the next player
            gameModel.NextTown();
            return;
        }
    }

    // Abstract Decorator
    public abstract class ControllerDecorator : ControllerComponent
    {
        protected ControllerComponent component;
        
        protected ControllerDecorator(GameModel gameModel, IGameView gameView) : base(gameModel, gameView)
        {

        }

        public void SetComponent(ControllerComponent component)
        {
            this.component = component;
        }
        
        public override void NextPlayer()
        {
            if (component != null)
            {
                component.NextPlayer();
            }

            List<string> townInfo;
            gameView.ShowTownInfo(GenerateTownInfoList(gameModel.player));

            // but I'll clear actions and info lists
            gameView.ShowCellInfo(null);
            gameView.ShowActions(null);
        }
    }

    // Concrete Decorator
    public class MultiplePlayersGameConrollerDecorator : ControllerDecorator
    {
        public MultiplePlayersGameConrollerDecorator(GameModel gameModel, IGameView gameView) : base(gameModel, gameView)
        {

        }

        public override void NextPlayer()
        {
            base.NextPlayer();
            // here we do not need to do extra work
        }

    }

    // Concrete Decorator
    public class AIGameConrollerDecorator : ControllerDecorator
    {
        public AIGameConrollerDecorator(GameModel gameModel, IGameView gameView) : base(gameModel, gameView)
        {
            
        }

        public override void NextPlayer()
        {
            base.NextPlayer();
            // Here we need to check if player that just gained the right to move 
            // is the AI. If it's not then just move on. If it is an AI then generate
            // a move and give up the right to move by calling NextPlayer()
            List<Position> moves = gameManager.GetPossibleMoves(gameModel.player);
            if (gameModel.player == 0)
            {
                // it is not an AI
                List<string> info = new List<string>();
                foreach (Position pos in moves)
                {
                    info.Add(pos.ToString());
                }
                gameView.ShowCellInfo(info);
                //if (gameModel.towns[(int)gameModel.player].mEnergy == 0)
                    //NextPlayer();
                return;
            }
            else
            {
                // it is AI, so generate a move
                for (int i = 0; i < gameModel.rows * gameModel.columns; i++)
                {
                    Random random = new Random();
                    if (moves.Count == 0)
                    {
                        // this player lost the game
                        return;
                    }

                    int subscript = random.Next(0, moves.Count);
                    int row = moves[subscript].row;
                    int col = moves[subscript].col;
                    Area area = gameModel.GetArea((uint)row, (uint)col);
                    List<string> actions, info;
                    actions = GenerateActionList((uint)row, (uint)col);
                    info = GenerateInfoList((uint)row, (uint)col);

                    if (actions.Count > 0)
                    {
                        if (actions.Contains("Capture Area"))
                        {
                            ActionSelected((uint)row, (uint)col, "Capture Area");
                            base.NextPlayer();
                            return;
                        }
                        else
                        {
                            if (actions.Count == 1 && actions[0].Equals("no actions available"))
                                continue;
                            int selection = random.Next(0, actions.Count - 1);
                            ActionSelected((uint)row, (uint)col, actions.ElementAt(selection));
                            base.NextPlayer();
                            return;
                        }
                    }
                }
            }

        }

    }

}
