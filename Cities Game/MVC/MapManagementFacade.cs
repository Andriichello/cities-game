﻿using Cities_Game.Areas;
using Cities_Game.Buildings;
using Cities_Game.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cities_Game.MVC
{
    public interface IMapManagementFacade
    {
        void GenerateMap(uint rows, uint columns, uint towns);
        bool RemoveBuilding(uint row, uint column);
        bool RemoveObstacle(uint row, uint column);
        bool PlaceBuilding(uint row, uint column, IFactory buildingFactory);
        bool UpgradeBuilding(uint row, uint column);
        bool CapureArea(uint row, uint column);

        List<Position> GetPossibleMoves(uint player);
    }

    public class MapManagementFacade : IMapManagementFacade
    {
        private readonly uint buildEnergyPrice = 1;
        private readonly uint upgradeEnergyPrice = 1;
        private readonly uint removalEnergyPrice = 1;
        private readonly uint captureEnergyPrice = 2;

        private static Random random = new Random();
        public static HouseFactory houseFactory { get; private set; } = new HouseFactory();
        public static WoodWorksFactory woodWorksFactory { get; private set; } = new WoodWorksFactory(); 
        public static RockWorksFactory rockWorksFactory { get; private set; } = new RockWorksFactory();
        public static FoodWorksFactory foodWorksFactory { get; private set; } = new FoodWorksFactory();
        private GameModel gameModel;

        public MapManagementFacade(GameModel gameModel)
        {
            this.gameModel = gameModel;
        }

        public void GenerateMap(uint rows, uint columns, uint towns)
        {
            gameModel.rows = rows;
            gameModel.columns = columns;

            gameModel.map = new List<Area>();

            for (int i = 0; i < rows * columns; i++)
            {
                if (i == 0 || i == (rows * columns - 1))
                {
                    Area root = new Area(null, houseFactory.CreateBuilding(), null);
                    gameModel.map.Add(root);
                    root.SetPosition(new Position((int)(i / rows), (int)(i % rows)));
                    continue;
                }

                // areas creation
                Resource resource = null;
                Obstacle obstacle = null;
                Building building = null;

                // optional creation of resource
                if (GenerateRandomBool() == true)
                {
                    resource = GenerateRandomResource(1000, 10000);

                    // optional creation of works building
                    if (GenerateRandomBool() == true)
                    {
                        if (resource.type == RESOURCE_TYPE.WOOD)
                        {
                            building = woodWorksFactory.CreateBuilding();
                        }
                        if (resource.type == RESOURCE_TYPE.ROCK)
                        {
                            building = rockWorksFactory.CreateBuilding();
                        }
                        if (resource.type == RESOURCE_TYPE.ROCK)
                        {
                            building = foodWorksFactory.CreateBuilding();
                        }
                    }
                }
                // optional creation of house
                else if (GenerateRandomBool() == true)
                {
                    building = houseFactory.CreateBuilding();
                }
                // optional creation of obstacle
                else if (GenerateRandomBool() == true)
                {
                    // optional creation of resource
                    if (GenerateRandomBool() == true)
                        resource = GenerateRandomResource(1000, 10000);
                    
                    obstacle = GenerateRandomObstacle(20, 100);
                }

                Area area = new Area(resource, building, obstacle);
                int row, col;
                row = (int)(i / rows);
                col = (int)(i % rows);
                area.SetPosition(new Position(row, col));
                gameModel.map.Add(area);
            }

            GenerateTowns(rows, columns, towns);
        }

        private void GenerateTowns(uint rows, uint columns, uint towns)
        {
            gameModel.towns = new List<Town>();
            // creating and populating towns
            for (int i = 0; i < towns; i++)
            {
                List<Resource> townResources = new List<Resource>();
                townResources.Add(new Resource(RESOURCE_TYPE.WOOD, 100));
                townResources.Add(new Resource(RESOURCE_TYPE.ROCK, 50));
                townResources.Add(new Resource(RESOURCE_TYPE.FOOD, 1000));

                Town town = new Town(townResources, null, 100);
                if (i == 0)
                {
                    town.AddToPositions(gameModel.map.ElementAt(0).GetPosition());
                    gameModel.map.ElementAt(0).Capture();
                }
                else if (i == 1)
                {
                    town.AddToPositions(gameModel.map.Last().GetPosition());
                    gameModel.map.Last().Capture();
                }

                gameModel.towns.Add(town);
            }

            // creating resources around root cells
            if (rows > 1 && columns > 1)
            {
                for (int i = 0; i < 2; i++)
                {
                    Resource wood = new Resource(RESOURCE_TYPE.WOOD, (uint)random.Next(1000, 5000));
                    Resource rock = new Resource(RESOURCE_TYPE.ROCK, (uint)random.Next(1000, 3000));
                    Resource food = new Resource(RESOURCE_TYPE.FOOD, (uint)random.Next(3000, 10000));
                    if (i == 0)
                    {
                        Area woodArea = new Area(wood, null, null);
                        Area rockArea = new Area(rock, null, null);
                        Area foodArea = new Area(food, null, null);
                        woodArea.SetPosition(gameModel.map[1].GetPosition());
                        gameModel.map[1] = woodArea;

                        rockArea.SetPosition(gameModel.map[(int)columns].GetPosition());
                        gameModel.map[(int)columns] = rockArea;

                        foodArea.SetPosition(gameModel.map[(int)columns + 1].GetPosition());
                        gameModel.map[(int)columns + 1] = foodArea;
                    }
                    else if (i == 1)
                    {
                        Area woodArea = new Area(wood, null, null);
                        Area rockArea = new Area(rock, null, null);
                        Area foodArea = new Area(food, null, null);
                        int last = gameModel.map.Count - 1;
                        woodArea.SetPosition(gameModel.map[last - 1].GetPosition());
                        gameModel.map[last - 1] = woodArea;

                        rockArea.SetPosition(gameModel.map[last - (int)columns].GetPosition());
                        gameModel.map[last - (int)columns] = rockArea;

                        foodArea.SetPosition(gameModel.map[last - (int)columns - 1].GetPosition());
                        gameModel.map[last - (int)columns - 1] = foodArea;
                    }
                }
            }
            }

        private bool GenerateRandomBool(uint range = 100)
        {
            return ((random.Next(0, (int)range) % 2) == 0);
        }

        private Resource GenerateRandomResource(uint minAmount, uint maxAmount)
        {
            uint amount = (uint)random.Next((int)minAmount, (int)maxAmount);
            RESOURCE_TYPE type = GenerateRandomResourceType();

            return new Resource(type, amount);
        }

        private RESOURCE_TYPE GenerateRandomResourceType()
        {
            RESOURCE_TYPE type = RESOURCE_TYPE.WOOD;
            if (GenerateRandomBool() == true)
                type = RESOURCE_TYPE.FOOD;
            else
            {
                if (GenerateRandomBool() == true)
                    type = RESOURCE_TYPE.WOOD;
                else
                    type = RESOURCE_TYPE.ROCK;
            }

            return type;
        }

        private Obstacle GenerateRandomObstacle(uint minAmount, uint maxAmount)
        {
            uint amount = (uint)random.Next((int)minAmount, (int)maxAmount);
            RESOURCE_TYPE type = GenerateRandomResourceType();

            return new Obstacle(new Resource(type, amount));
        }

        public bool PlaceBuilding(uint row, uint column, IFactory buildingFactory)
        {
            Town town = gameModel.towns.ElementAt((int)gameModel.player);
            if (town.mEnergy < buildEnergyPrice)
                return false;

            Area area = gameModel.GetArea(row, column);
            if (area == null)
                return false;

            Building building = buildingFactory.CreateBuilding();
            if (town.IsThereEnoughtResources(building.GetPrice(1)) == false)
                return false;
            area.PlaceBuilding(building);
            town.DecrementResources(building.GetPrice(1));
            town.DecrementEnergy(buildEnergyPrice);
            return true;
        }

        public bool CapureArea(uint row, uint col)
        {
            Town town = gameModel.towns.ElementAt((int)gameModel.player);
            if (town.mEnergy < captureEnergyPrice)
                return false;

            Area area = gameModel.GetArea(row, col);
            if (area == null)
                return false;

            if (gameModel.GetAvailableArea(gameModel.player).Contains(area))
            {
                if (area.Capture())
                {
                    gameModel.towns[(int)gameModel.player].AddToPositions(area.GetPosition());
                    town.DecrementEnergy(captureEnergyPrice);
                    return true;
                }
            }
            return false;
        }

        public bool RemoveBuilding(uint row, uint col)
        {
            Town town = gameModel.towns.ElementAt((int)gameModel.player);
            if (town.mEnergy < removalEnergyPrice)
                return false;

            Area area = gameModel.GetArea(row, col);
            if (area == null)
                return false;
            area.RemoveBuilding();
            town.DecrementEnergy(removalEnergyPrice);
            return true;
        }

        public bool RemoveObstacle(uint row, uint col)
        {
            Town town = gameModel.towns.ElementAt((int)gameModel.player);
            if (town.mEnergy < removalEnergyPrice)
                return false;
            Area area = gameModel.GetArea(row, col);
            if (area == null)
                return false;

            Resource extra = area.RemoveObstacle().GetResource();
            if (extra != null)
                town.AddToResources(extra);
            town.DecrementEnergy(removalEnergyPrice);
            return true;
        }

        public List<Position> GetPossibleMoves(uint player)
        {
            List<Area> moves = gameModel.GetAvailableArea(player);
            List<Position> positions = new List<Position>();
            foreach (Area area in moves)
            {
                positions.Add(area.GetPosition());
            }
            return positions;
        }

        public bool UpgradeBuilding(uint row, uint column)
        {
            Town town = gameModel.towns.ElementAt((int)gameModel.player);
            if (town.mEnergy < upgradeEnergyPrice)
                return false;
            Area area = gameModel.GetArea(row, column);
            if (area == null)
                return false;

            Building building = area.GetBuilding();
            bool isSuccessfull = building.Upgrade(town.GetResources());
            
            if (isSuccessfull)
            {
                town.DecrementEnergy(upgradeEnergyPrice);
                town.DecrementResources(building.GetPrice(building.GetLevel()));
            }
            return isSuccessfull;
        }
    }
}
