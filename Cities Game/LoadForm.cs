﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cities_Game
{
    public partial class LoadForm : Form
    {
        private string savesFileName = "saves.txt";
        private ILoadRequester requester;
        public LoadForm(ILoadRequester requester)
        {
            InitializeComponent();
            this.requester = requester;
        }

        private void LoadForm_Load(object sender, EventArgs e)
        {
            if (File.Exists(savesFileName) == false)
                return;

            StreamReader streamReader = new StreamReader(savesFileName);
            string line = streamReader.ReadLine();
            while (line != null)
            {
                lbSaveNames.Items.Add(line);
                line = streamReader.ReadLine();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (lbSaveNames.SelectedItem == null)
                return;
            string line = lbSaveNames.SelectedItem.ToString();
            string mode = "single";
            if (line.Contains("multiple "))
                mode = "multiple";

            int subscript = line.IndexOf(" ");
            if (subscript > 0)
                line = line.Substring(subscript + 1, line.Length - subscript - 1);

            requester.LoadNameSelected(line, mode);
            Close();
        }

        private void LoadForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }
    }
}
