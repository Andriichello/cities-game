﻿namespace Cities_Game
{
    partial class HelpCellsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbCellsInfo = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbCellsInfo
            // 
            this.tbCellsInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbCellsInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbCellsInfo.Enabled = false;
            this.tbCellsInfo.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tbCellsInfo.Location = new System.Drawing.Point(0, 0);
            this.tbCellsInfo.Multiline = true;
            this.tbCellsInfo.Name = "tbCellsInfo";
            this.tbCellsInfo.Size = new System.Drawing.Size(384, 111);
            this.tbCellsInfo.TabIndex = 0;
            this.tbCellsInfo.Text = "Each letter in a cell has a meaning.\r\nR - Resource\r\nO - Obstracle\r\nB - Building\r\n" +
    "Press Esc to exit...";
            // 
            // HelpCellsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 111);
            this.ControlBox = false;
            this.Controls.Add(this.tbCellsInfo);
            this.KeyPreview = true;
            this.MaximumSize = new System.Drawing.Size(400, 150);
            this.MinimumSize = new System.Drawing.Size(400, 150);
            this.Name = "HelpCellsForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cells";
            this.Load += new System.EventHandler(this.HelpCellsForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.HelpCellsForm_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbCellsInfo;
    }
}