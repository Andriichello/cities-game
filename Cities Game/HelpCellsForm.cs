﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cities_Game
{
    public partial class HelpCellsForm : Form
    {
        public HelpCellsForm()
        {
            InitializeComponent();
        }

        private void HelpCellsForm_Load(object sender, EventArgs e)
        {

        }

        private void HelpCellsForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }
    }
}
