﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cities_Game.Resources
{
    [Serializable]
    public class Obstacle
    {
        private Resource mResource;

        public Obstacle(Resource mResource)
        {
            this.mResource = mResource;
        }

        public Obstacle(Obstacle toCopy)
        {
            this.mResource = new Resource(toCopy.mResource);
        }

        public Resource GetResource()
        {
            return mResource;
        }
    }

}
