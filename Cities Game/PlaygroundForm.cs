﻿using Cities_Game.Areas;
using Cities_Game.MVC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cities_Game
{
    public interface IGameView
    {
        void ClearCell(Position position);
        void ShowCell(Position position, string text);

        void ClearMap();
        void ShowMap(List<List<string>> map);

        void ShowTown(List<Position> areas, uint player);

        void ShowCellInfo(List<string> info);
        void ShowTownInfo(List<string> info);
        void ShowActions(List<string> actions);
    }

    public interface ILoadRequester
    {
        void LoadNameSelected(string fileName, string loadName);
    }

    public interface ISaveRequester
    {
        void SaveNameSelected(string fileName);
        string GetMode();
    }

    public partial class PlaygroundForm : Form, IGameView, ILoadRequester, ISaveRequester
    {
        private string fileName = "lastSave.dat";
        private ControllerDecorator gameController;
        private Form parent;
        
        public PlaygroundForm(Form parent = null)
        {
            this.parent = parent;
            this.gameController = null;
            InitializeComponent();
            SetUpGrid();
        }

        public void SetGameConroller(ControllerDecorator controller)
        {
            this.gameController = controller;
        }

        public void GenerateMap(uint rows, uint columns)
        {
            if (gameController != null)
                gameController.CreateNewGame(rows, columns, 2);
        }

        #region Form
        private void SetUpGrid()
        {
            mapGridView.AllowUserToAddRows = false;
            mapGridView.AllowUserToResizeColumns = false;
            mapGridView.AllowUserToResizeRows = false;

            mapGridView.ColumnHeadersVisible = false;
            mapGridView.RowHeadersVisible = false;
            mapGridView.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            mapGridView.GridColor = Color.Black;
        }

        private void lbActions_DoubleClick(object sender, EventArgs e)
        {
            if (mapGridView.SelectedCells.Count == 0)
                return;

            if (lbActions.SelectedItem == null)
                return;

            gameController.ActionSelected((uint)mapGridView.SelectedCells[0].RowIndex, (uint)mapGridView.SelectedCells[0].ColumnIndex, lbActions.SelectedItem.ToString());
        }

        private void mapGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (mapGridView.SelectedCells.Count == 0)
                return;

            gameController.CellSelected((uint)mapGridView.SelectedCells[0].RowIndex, (uint)mapGridView.SelectedCells[0].ColumnIndex);
        }

        private void btnGiveTurn_Click(object sender, EventArgs e)
        {
            if (gameController != null)
                gameController.NextPlayer();
        }

        private void PlaygroundForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                if (gameController != null)
                    gameController.RevertToPreviousSave();
            }

            if (e.KeyCode == Keys.F2)
            {
                if (gameController != null)
                    gameController.SaveGame();
            }

            if (e.KeyCode == Keys.F3)
            {
                if (gameController != null)
                {
                    var formatter = new BinaryFormatter();

                    // Restore from file
                    FileStream stream = File.OpenRead(fileName);
                    GameModel.Memento memento = (GameModel.Memento)formatter.Deserialize(stream);
                    stream.Close();
                    gameController.LoadGame(memento);
                }
            }

            if (e.KeyCode == Keys.F4)
            {
                if (gameController != null)
                {
                    GameModel.Memento memento = gameController.SaveGame();

                    FileStream stream = File.Create(fileName);
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(stream, memento);
                    stream.Close();
                }
            }


        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            parent.Close();
            base.OnFormClosing(e);
        }

        private void btnGiveUpTurn_Click(object sender, EventArgs e)
        {
            if (gameController != null)
            {
                gameController.NextPlayer();
                mapGridView.ClearSelection();
            }
        }
        #endregion

        #region IGameView Implementations
        public void UpdateCell(uint row, uint col, string text, int player)
        {
            Color color = Color.FromArgb(244, 255, 100);
            if (player == 1)
                color = Color.FromArgb(22, 155, 200);

            if (text.Length > 0 && text[0] == 'C')
                mapGridView.Rows[(int)row].Cells[(int)col].Style.BackColor = color;
            //Color.FromArgb(244, 255, 183);
            mapGridView.Rows[(int)row].Cells[(int)col].Selected = false;
            mapGridView.Rows[(int)row].Cells[(int)col].Value = text;
        }

        public void ShowMap(List<List<string>> map)
        {
            int sideSize = mapGridView.Width / map[0].Count;

            mapGridView.ColumnCount = (int)map[0].Count;
            mapGridView.Rows.Clear();
            for (int i = 0; i < map.Count; i++)
            {
                string[] rowData = new string[map[i].Count];
                for (int j = 0; j < map[i].Count; j++)
                {
                    rowData[j] = map[i][j];
                }
                mapGridView.Rows.Add(rowData);
                mapGridView.Rows[i].Height = sideSize;
            }

            foreach (DataGridViewColumn column in mapGridView.Columns)
            {
                column.Width = sideSize;
            }
        }

        public void ClearMap()
        {
            for (int i = 0; i < mapGridView.Rows.Count; i++)
            {
                for (int j = 0; j < mapGridView.Rows[i].Cells.Count; j++)
                {
                    mapGridView.Rows[i].Cells[j].Value = "";
                    mapGridView.Rows[i].Cells[j].Style.BackColor = Color.White;
                }
            }
        }

        public void ShowCell(Position position, string text)
        {
            if (mapGridView.Rows.Count <= position.row)
                return;

            if (mapGridView.Rows[position.row].Cells.Count <= position.col)
                return;

            mapGridView.Rows[position.row].Cells[position.col].Value = text;
        }

        public void ClearCell(Position position)
        {
            mapGridView.Rows[position.row].Cells[position.col].Value = "";
            mapGridView.Rows[position.row].Cells[position.col].Style.BackColor = Color.White;
        }

        public void ShowCellInfo(List<string> info)
        {
            lbInfo.Items.Clear();
            if (info == null)
                return;

            foreach (string str in info)
                lbInfo.Items.Add(str);
        }

        public void ShowTown(List<Position> areas, uint player)
        {
            //Color color = Color.FromArgb(244, 255, 100);  
            Color color = Color.FromArgb(100, 250, 140);

            if (player == 1)
                color = Color.FromArgb(255, 200, 90);
                //color = Color.FromArgb(22, 155, 200);

            foreach (Position pos in areas)
                mapGridView.Rows[pos.row].Cells[pos.col].Style.BackColor = color;
        }
        
        public void ShowTownInfo(List<string> info)
        {
            lbTown.Items.Clear();
            if (info == null)
                return;

            foreach (string sentence in info)
            {
                lbTown.Items.Add(sentence);
            }
        }

        public void ShowActions(List<string> actions)
        {
            lbActions.Items.Clear();
            if (actions == null)
                return;

            foreach (string sentence in actions)
            {
                lbActions.Items.Add(sentence);
            }
        }
        #endregion

        #region IRequesters
        public void LoadNameSelected(string fileName, string loadMode)
        {
            var formatter = new BinaryFormatter();

            // Restore from file
            FileStream stream = File.OpenRead(fileName);
            GameModel.Memento memento = (GameModel.Memento)formatter.Deserialize(stream);
            stream.Close();

            // TODO: solve problem in this function
            if (loadMode == "single")
            {
                if (gameController == null || gameController as AIGameConrollerDecorator == null)
                {
                    GameModel model = new GameModel();
                    model.RevertToMemento(memento);
                    gameController = new AIGameConrollerDecorator(model, this);
                    gameController.SetComponent(new DefaultGameController(model, this));
                }
                gameController.LoadGame(memento);
                return;
            }
            else if (loadMode == "multiple")
            {
                if (gameController == null || gameController as MultiplePlayersGameConrollerDecorator == null)
                {
                    GameModel model = new GameModel();
                    model.RevertToMemento(memento);
                    gameController = new MultiplePlayersGameConrollerDecorator(model, this);
                    gameController.SetComponent(new DefaultGameController(model, this));
                }
                gameController.LoadGame(memento);
                return;
            }
            else
                gameController.LoadGame(memento);
        }

        public void SaveNameSelected(string fileName)
        {
            if (gameController == null)
                return;
            GameModel.Memento memento = gameController.SaveGame();

            FileStream stream = File.Create(fileName);
            var formatter = new BinaryFormatter();
            formatter.Serialize(stream, memento);
            stream.Close();
        }

        public string GetMode()
        {
            if (gameController == null)
                return "single";

            if (gameController as AIGameConrollerDecorator != null)
            {
                return "single";
            }
            if (gameController as MultiplePlayersGameConrollerDecorator != null)
            {
                return "multiple";
            }

            return "single";
        }
        #endregion 

        #region STRIP MENU
        private void saveToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (gameController != null)
            {
                gameController.SaveGame();
            }
        }

        private void revertToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (gameController != null)
            {
                gameController.RevertToPreviousSave();
            }
        }

        private void cellsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form form = new HelpCellsForm();
            form.Show();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveForm form = new SaveForm(this);
            form.Show();
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadForm form = new LoadForm(this);
            form.Show();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Hide();
            parent.Show();
        }
        #endregion
    }
}
