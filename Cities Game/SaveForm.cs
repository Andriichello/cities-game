﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cities_Game.MVC
{
    public partial class SaveForm : Form
    {
        private string fileName = "saves.txt";
        private ISaveRequester requester;

        public SaveForm(ISaveRequester requester)
        {
            InitializeComponent();
            this.requester = requester;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            string mode = requester.GetMode();
            string newSaveName = tbFileName.Text;
            if (newSaveName.Length == 0)
                return;

            newSaveName += ".dat";
            if (File.Exists(fileName) == true)
            {
                StreamReader streamReader = new StreamReader(fileName);
                string content = streamReader.ReadToEnd();
                streamReader.Close();

                if (content.Contains(newSaveName))
                    return;
                StreamWriter streamWriter = new StreamWriter(fileName);
                streamWriter.Write(content);
                streamWriter.WriteLine(mode +  " " + newSaveName);
                streamWriter.Close();
            }
            else
            {
                StreamWriter streamWriter = new StreamWriter(fileName);
                streamWriter.WriteLine(mode + " " + newSaveName);
                streamWriter.Close();
            }

            requester.SaveNameSelected(newSaveName);
            Close();
        }

        private void SaveForm_Load(object sender, EventArgs e)
        {

        }

        private void SaveForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }
    }
}
