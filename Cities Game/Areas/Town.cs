﻿using Cities_Game.MVC;
using Cities_Game.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cities_Game.Areas
{
    public interface ITownState : ICloneable
    {
        void ObtainIncome(GameModel model);
        void IncreaseEnergy();
        void IncreasePopulation();
        void SetTown(Town town);
    }

    [Serializable]
    public class Town : ICloneable
    {
        #region State Pattern
        private ITownState mCurrentState;
        [Serializable]
        public class StarvingTown : ITownState
        {
            private Town town;
            public StarvingTown(Town town)
            {
                this.town = town;
            }

            public void ObtainIncome(GameModel model)
            {
                uint percents = 50;
                foreach (Position pos in town.mPositions)
                {
                    Area area = model.GetArea(pos);
                    if (area != null && area.GetBuilding() != null)
                    {
                        town.AddToResources(area.GetBuilding().ObtainIncome(percents));
                    }
                }

                Resource food = null;
                for (int i = 0; i < town.mResources.Count; i++)
                {
                    if (town.mResources[i].type == RESOURCE_TYPE.FOOD)
                    {
                        food = town.mResources[i];
                        break;
                    }
                }

                if (food != null)
                {
                    if (food.amount > town.mPopulation * 1.5)
                    {
                        town.mCurrentState = new SatisfiedTown(town);
                    }
                }
            }
         
            public void IncreaseEnergy()
            {
                town.mEnergy += 1;
            }

            public void IncreasePopulation()
            {
                int newPopulation = (int)town.mPopulation;
                // calculations here
                double decrement = 0;
                decrement = newPopulation * 0.03;

                if (decrement < 1.0)
                {
                    decrement = 1;
                }
                newPopulation -= (int)decrement;
                if (newPopulation < 0)
                    newPopulation = 0;

                town.mPopulation = (uint)newPopulation;
            }

            public void SetTown(Town town)
            {
                this.town = town;
            }

            public object Clone()
            {
                return this.MemberwiseClone();
            }
        }

        [Serializable]
        public class SatisfiedTown : ITownState
        {
            private Town town;
            public SatisfiedTown(Town town)
            {
                this.town = town;
            }

            public void ObtainIncome(GameModel model)
            {
                uint percents = 100;
                foreach (Position pos in town.mPositions)
                {
                    Area area = model.GetArea(pos);
                    if (area != null && area.GetBuilding() != null)
                    {
                        town.AddToResources(area.GetBuilding().ObtainIncome(percents));
                    }
                }

                Resource food = null;
                for (int i = 0; i < town.mResources.Count; i++)
                {
                    if (town.mResources[i].type == RESOURCE_TYPE.FOOD)
                    {
                        food = town.mResources[i];
                        break;
                    }
                }

                if (food != null)
                {
                    if (food.amount < town.mPopulation)
                    {
                        town.mCurrentState = new StarvingTown(town);
                    }
                }
            }

            public void IncreaseEnergy()
            {
                town.mEnergy += 2;
            }

            public void IncreasePopulation()
            {
                uint newPopulation = town.mPopulation;
                // calculations here
                double increment = 0;
                if (newPopulation < 20)
                {
                    increment = newPopulation * 0.3;
                }
                else
                {
                    increment = newPopulation * 0.15;
                }

                if (increment < 1.0)
                {
                    increment = 1;
                }
                newPopulation += (uint)increment;
                town.mPopulation = newPopulation;
            }

            public void SetTown(Town town)
            {
                this.town = town;
            }

            public object Clone()
            {
                return this.MemberwiseClone();
            }
        }
        #endregion

        private List<Resource> mResources;
        private List<Position> mPositions;
        private uint mPopulation;
        public uint mEnergy { get; private set; }

        private Town()
        {
            mResources = new List<Resource>();
            mResources.Add(new Resource(RESOURCE_TYPE.WOOD, 0));
            mResources.Add(new Resource(RESOURCE_TYPE.ROCK, 0));
            mResources.Add(new Resource(RESOURCE_TYPE.FOOD, 0));
            mPositions = new List<Position>();

            mPopulation = 100;
            mEnergy = 2;
        }

        public Town(List<Resource> mResources, List<Position> mPositions, uint mPopulation)
            : this()
        {
            if (mResources != null)
            {
                foreach (Resource given in mResources)
                {
                    foreach (Resource current in this.mResources)
                    {
                        if (current != null && current.type == given.type)
                        {
                            current.IncreaseAmount(given.amount);
                            break;
                        }
                    }
                }
            }
            
            if (mPositions != null)
            {
                foreach (Position area in mPositions)
                {
                    this.mPositions.Add(area);
                }
            }
            
            this.mPopulation = mPopulation;
            this.mCurrentState = new SatisfiedTown(this);
        }

        public List<Resource> GetResources()
        {
            return mResources;
        }

        public uint GetPopulation()
        {
            return mPopulation;
        }

        public uint CountWorkingPopulation(GameModel model)
        {
            uint count = 0;
            foreach (Position pos in mPositions)
            {
                Area area = model.GetArea(pos);
                if (area != null && area.GetBuilding() != null)
                {
                    count += area.GetBuilding().GetWorkers();
                }
            }

            return count;
        }
        
        private void IncrementPopulation()
        {
            mCurrentState.IncreasePopulation();
        }
        
        public void AddToResources(Resource income)
        {
            if (income == null)
                return;

            foreach (Resource resource in mResources)
            {
                if (resource != null && resource.type == income.type)
                {
                    resource.IncreaseAmount(income.amount);
                    break;
                }
            }
        }

        private void IncrementResources(GameModel model)
        {
            mCurrentState.ObtainIncome(model);
        }

        public void AddToPositions(Position position)
        {
            mPositions.Add(position);
        }

        public List<Position> GetPositions()
        {
            return mPositions;
        }

        public void IncrementEnergy()
        {
            mCurrentState.IncreaseEnergy();
        }

        public void DecrementEnergy(uint amount)
        {
            if (amount > mEnergy)
                mEnergy = 0;
            else
                mEnergy -= amount;
        }

        public bool IsThereEnoughtResources(List<Resource> price)
        {
            bool isEnought = true;
            foreach (Resource r in mResources)
            {
                foreach (Resource p in price)
                {
                    if (r.type == p.type)
                    {
                        if (r.amount < p.amount)
                            return false;
                    }
                }
            }

            return isEnought;
        }

        public void DecrementResources(List<Resource> price)
        {
            foreach (Resource r in mResources)
            {
                foreach (Resource p in price)
                {
                    if (r.type == p.type)
                    {
                        int newAmount = (int)(r.amount - p.amount);
                        r.ResetAmount();
                        r.IncreaseAmount((uint)newAmount);
                    }
                }
            }
        }

        public uint GetEnergy()
        {
            return this.mEnergy;
        }

        private void DecrementFood()
        {
            foreach (Resource res in mResources)
            {
                if (res.type == RESOURCE_TYPE.FOOD)
                {
                    int newAmount = (int)res.amount - (int)mPopulation;

                    if (newAmount < 0)
                        newAmount = 0;
                    res.ResetAmount();
                    res.IncreaseAmount((uint)newAmount);
                }
            }
        }

        public void OnDayEnd(GameModel model)
        {
            foreach (Position pos in mPositions)
            {
                Area area = model.GetArea(pos);

                if (area != null && area.GetBuilding() != null)
                {
                    area.GetBuilding().OnDayEnd();
                }
            }

            DecrementFood();
            IncrementResources(model);
            IncrementPopulation();
            IncrementEnergy();
        }

        public object Clone()
        {
            Town town = (Town)MemberwiseClone();
            town.mPositions = new List<Position>();
            foreach (Position pos in mPositions)
                town.mPositions.Add(pos.Clone());

            town.mCurrentState = (ITownState)mCurrentState.Clone();
            town.mCurrentState.SetTown(town);

            town.mResources = new List<Resource>();
            foreach (Resource res in mResources)
                town.mResources.Add(new Resource(res));

            return town;
        }
    }

}
