﻿using Cities_Game;
using Cities_Game.MVC;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cities_Game
{
    public partial class MenuForm : Form, ILoadRequester
    {
        public MenuForm()
        {
            InitializeComponent();
        }

        private void btnNewGame_Click(object sender, EventArgs e)
        {
            PlaygroundForm form = new PlaygroundForm(this);
            GameModel gameModel = new GameModel();
            AIGameConrollerDecorator gameController = new AIGameConrollerDecorator(gameModel, form);
            gameController.SetComponent(new DefaultGameController(gameModel, form));
            form.SetGameConroller(gameController);
            form.GenerateMap(9, 9);
            form.Show();
            this.Hide();
        }

        private void btnMultiple_Click(object sender, EventArgs e)
        {
            PlaygroundForm form = new PlaygroundForm(this);
            GameModel gameModel = new GameModel();
            MultiplePlayersGameConrollerDecorator gameController = new MultiplePlayersGameConrollerDecorator(gameModel, form);
            gameController.SetComponent(new DefaultGameController(gameModel, form));
            form.SetGameConroller(gameController);
            form.GenerateMap(9, 9);
            form.Show();
            this.Hide();
        }

        private void MenuForm_Load(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            LoadFromStartForm form = new LoadFromStartForm(this);
            form.Show();
        }

        public void LoadNameSelected(string fileName, string loadMode)
        {
            PlaygroundForm form = new PlaygroundForm(this);
            GameModel gameModel = new GameModel();

            ControllerDecorator gameController;
            if (loadMode == "single")
            {
                gameController = new AIGameConrollerDecorator(gameModel, form);
            }
            else
            {
                gameController = new MultiplePlayersGameConrollerDecorator(gameModel, form);
            }
            gameController.SetComponent(new DefaultGameController(gameModel, form));
            form.SetGameConroller(gameController);
            form.LoadNameSelected(fileName, loadMode);
            form.Show();
            this.Hide();
        }
    }


}
