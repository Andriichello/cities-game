﻿namespace Cities_Game
{
    partial class PlaygroundForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.mapGridView = new System.Windows.Forms.DataGridView();
            this.gameMenu = new System.Windows.Forms.MenuStrip();
            this.gameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.revertToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cellsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbTown = new System.Windows.Forms.ListBox();
            this.lbInfo = new System.Windows.Forms.ListBox();
            this.lbActions = new System.Windows.Forms.ListBox();
            this.btnGiveUpTurn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.mapGridView)).BeginInit();
            this.gameMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // mapGridView
            // 
            this.mapGridView.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.mapGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.mapGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.mapGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.mapGridView.ColumnHeadersHeight = 20;
            this.mapGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.mapGridView.ColumnHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.GradientActiveCaption;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.mapGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.mapGridView.Location = new System.Drawing.Point(13, 31);
            this.mapGridView.Margin = new System.Windows.Forms.Padding(4);
            this.mapGridView.MultiSelect = false;
            this.mapGridView.Name = "mapGridView";
            this.mapGridView.ReadOnly = true;
            this.mapGridView.RowHeadersWidth = 30;
            this.mapGridView.RowTemplate.Height = 20;
            this.mapGridView.Size = new System.Drawing.Size(490, 490);
            this.mapGridView.TabIndex = 6;
            this.mapGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.mapGridView_CellClick);
            // 
            // gameMenu
            // 
            this.gameMenu.BackColor = System.Drawing.Color.Transparent;
            this.gameMenu.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.gameMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gameToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.revertToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.gameMenu.Location = new System.Drawing.Point(0, 0);
            this.gameMenu.Name = "gameMenu";
            this.gameMenu.Size = new System.Drawing.Size(884, 27);
            this.gameMenu.TabIndex = 15;
            // 
            // gameToolStripMenuItem
            // 
            this.gameToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveAsToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.gameToolStripMenuItem.Name = "gameToolStripMenuItem";
            this.gameToolStripMenuItem.Size = new System.Drawing.Size(57, 23);
            this.gameToolStripMenuItem.Text = "Game";
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(141, 24);
            this.saveAsToolStripMenuItem.Text = "Save As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(141, 24);
            this.loadToolStripMenuItem.Text = "Load";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(141, 24);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(57, 23);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click_1);
            // 
            // revertToolStripMenuItem
            // 
            this.revertToolStripMenuItem.Name = "revertToolStripMenuItem";
            this.revertToolStripMenuItem.Size = new System.Drawing.Size(75, 23);
            this.revertToolStripMenuItem.Text = "Revert";
            this.revertToolStripMenuItem.Click += new System.EventHandler(this.revertToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cellsToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(57, 23);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // cellsToolStripMenuItem
            // 
            this.cellsToolStripMenuItem.Name = "cellsToolStripMenuItem";
            this.cellsToolStripMenuItem.Size = new System.Drawing.Size(123, 24);
            this.cellsToolStripMenuItem.Text = "Cells";
            this.cellsToolStripMenuItem.Click += new System.EventHandler(this.cellsToolStripMenuItem_Click);
            // 
            // lbTown
            // 
            this.lbTown.BackColor = System.Drawing.SystemColors.Window;
            this.lbTown.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbTown.FormattingEnabled = true;
            this.lbTown.ItemHeight = 19;
            this.lbTown.Location = new System.Drawing.Point(519, 31);
            this.lbTown.Margin = new System.Windows.Forms.Padding(4);
            this.lbTown.Name = "lbTown";
            this.lbTown.Size = new System.Drawing.Size(352, 152);
            this.lbTown.TabIndex = 21;
            this.lbTown.DoubleClick += new System.EventHandler(this.lbActions_DoubleClick);
            // 
            // lbInfo
            // 
            this.lbInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbInfo.FormattingEnabled = true;
            this.lbInfo.ItemHeight = 19;
            this.lbInfo.Location = new System.Drawing.Point(518, 190);
            this.lbInfo.Margin = new System.Windows.Forms.Padding(4);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(353, 114);
            this.lbInfo.TabIndex = 20;
            this.lbInfo.DoubleClick += new System.EventHandler(this.lbActions_DoubleClick);
            // 
            // lbActions
            // 
            this.lbActions.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbActions.FormattingEnabled = true;
            this.lbActions.ItemHeight = 19;
            this.lbActions.Location = new System.Drawing.Point(518, 350);
            this.lbActions.Margin = new System.Windows.Forms.Padding(4);
            this.lbActions.Name = "lbActions";
            this.lbActions.Size = new System.Drawing.Size(353, 171);
            this.lbActions.TabIndex = 19;
            this.lbActions.DoubleClick += new System.EventHandler(this.lbActions_DoubleClick);
            // 
            // btnGiveUpTurn
            // 
            this.btnGiveUpTurn.Location = new System.Drawing.Point(519, 311);
            this.btnGiveUpTurn.Name = "btnGiveUpTurn";
            this.btnGiveUpTurn.Size = new System.Drawing.Size(352, 32);
            this.btnGiveUpTurn.TabIndex = 22;
            this.btnGiveUpTurn.Text = "Give Up Turn";
            this.btnGiveUpTurn.UseVisualStyleBackColor = true;
            this.btnGiveUpTurn.Click += new System.EventHandler(this.btnGiveUpTurn_Click);
            // 
            // PlaygroundForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 536);
            this.ControlBox = false;
            this.Controls.Add(this.btnGiveUpTurn);
            this.Controls.Add(this.lbTown);
            this.Controls.Add(this.lbInfo);
            this.Controls.Add(this.lbActions);
            this.Controls.Add(this.mapGridView);
            this.Controls.Add(this.gameMenu);
            this.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.KeyPreview = true;
            this.MainMenuStrip = this.gameMenu;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximumSize = new System.Drawing.Size(900, 575);
            this.MinimumSize = new System.Drawing.Size(900, 575);
            this.Name = "PlaygroundForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cities Game";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.PlaygroundForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.mapGridView)).EndInit();
            this.gameMenu.ResumeLayout(false);
            this.gameMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView mapGridView;
        private System.Windows.Forms.MenuStrip gameMenu;
        private System.Windows.Forms.ToolStripMenuItem gameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ListBox lbTown;
        private System.Windows.Forms.ListBox lbInfo;
        private System.Windows.Forms.ListBox lbActions;
        private System.Windows.Forms.Button btnGiveUpTurn;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem revertToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cellsToolStripMenuItem;
    }
}