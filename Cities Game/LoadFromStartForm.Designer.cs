﻿namespace Cities_Game
{
    partial class LoadFromStartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbSaveNames = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lbSaveNames
            // 
            this.lbSaveNames.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lbSaveNames.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbSaveNames.FormattingEnabled = true;
            this.lbSaveNames.ItemHeight = 19;
            this.lbSaveNames.Location = new System.Drawing.Point(0, 0);
            this.lbSaveNames.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lbSaveNames.Name = "lbSaveNames";
            this.lbSaveNames.Size = new System.Drawing.Size(384, 411);
            this.lbSaveNames.TabIndex = 0;
            this.lbSaveNames.DoubleClick += new System.EventHandler(this.tbSaveNames_DoubleClick);
            // 
            // LoadFromStartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 411);
            this.ControlBox = false;
            this.Controls.Add(this.lbSaveNames);
            this.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "LoadFromStartForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LoadFromStartForm";
            this.Load += new System.EventHandler(this.LoadFromStartForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.LoadFromStartForm_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lbSaveNames;
    }
}